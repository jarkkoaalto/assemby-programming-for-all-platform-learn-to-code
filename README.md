# Assemby Programming For All Platform - Learn To Code #

About this course

Assembly is the foundation for all applications, mobile to desktop. It is used in Raspberry Pi, Arm, Intel and much more

## Introduction 

live compiler: https://rextester.com/MQCXO12095

## Section 02: Registers 

Assemybly language Memory segment

1. Data Segment
2. Code Segment
3. Stack

Processor Register:

- 6 16bit processor registers
- 10 32bit processor registers
- General registers
- Control Registers
- Segment Registers
- Data Registers
- Pointer Registers
- Index Register

### Data Registers 

| 32-bit registers |        |     | 16-bit registers|
|------------------|---------|-----------| ------------------|
|EAX      | AH   |   AL| AX Accumilator |
|EBX      | BH   |   BL| BX Base        |
|ECX      | CH   |   CL| CX Counter     |
|EDX      | DH   |   DL| DX Data        |

### Pointer Register 

Index registers:

|       | 31    16|15   0|                    |
|-------|---------|------|--------------------|
| ESI   |         |  SI  | Source Index       |
| EDI   |         |  DI  | Desrtination index |

Pointer registers:

|       | 31    16|15   0|                    |
|-------|---------|------|--------------------|
| ESP   |         |  SP  | Stack pointer      |
| EDP   |         |  BP  | Base pointer       |

Index registers:

Control Registers:

- Overflow Flag (OF)
- Direction Flag (DF)
- Interrupt Flag (IF)
- Trap Flag (TF) 
- Sign Flag (SF) 
- Zero Flag (ZF) 
- Auxiliary Carry Flag (AF) 
- Parity Flag (PF) 
- Carry Flag (CF)

Segment registers:

- Code 
- Stack 
- data

## Section 03: Address Modes 

Register Addressing:

- move ebx 1 - Standard output
- move ecx, hello - put the variable ecx
- move edx, helloLen - print all content what  string containing
- int 80h - call the kernel
- mov eax,1 - Data register system call exit
- int 80h - return code of 0 (no error)

## Extras Learning Resourses ##

- hackr.io https://hackr.io/tutorials/learn-assembly-language
- WhoIsHostingThis https://www.whoishostingthis.com/resources/assembly-language/
- pravaraengg http://www.pravaraengg.org.in/Download/MA/assembly_tutorial.pdf
- gitconnected https://gitconnected.com/learn/assembly-language
- asmtutor https://asmtutor.com/
- Project Nayuki https://www.nayuki.io/page/a-fundamental-introduction-to-x86-assembly-programming
- Win32 Assembly Tutorials http://win32assembly.programminghorizon.com/tutorials.html