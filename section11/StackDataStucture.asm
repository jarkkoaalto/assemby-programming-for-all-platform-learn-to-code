;nasm 20.05.19
BITS 32

section .data
    hello: db 'Hello world!',10;
    helloLen: equ $-hello ;
  
section .text
	global _start

_start:
    mov ebp, 10
    mov esi, 5
    
    call epicPrint
    call epicPrint
    call epicPrint
    call epicPrint
    call epicPrint
    

	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;
 
; Function
epicPrint:
    push ebp
    push esi
    pop ebp
    pop esi
    
    mov eax,4            ; The system call for write (sys_write)
	mov ebx,1            ; File descriptor 1 - standard output
	mov ecx,hello        ; Put the offset of hello in ecx
	mov edx, ebp    ; zerosLen is a constant, so we don't need to say
	                     ;  mov edx,[helloLen] to get it's actual value
	int 80h             ; Call the kernel
    
 ret
 
 ; Print : HelloHello worlHelloHello worlHello