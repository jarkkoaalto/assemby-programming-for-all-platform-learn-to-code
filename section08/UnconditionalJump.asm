

;nasm 20.05.19


section .data
    hello: db 'Hello world!',10;
    helloLen: equ $-hello ;
    
section .text
	global _start

_start:
    mov esp, 1
    LP1:
    inc esp
    cmp esp, 5 ; compare
    
	mov eax,4            ; The system call for write (sys_write)
	mov ebx,1            ; File descriptor 1 - standard output
	mov ecx,hello        ; Put the offset of hello in ecx
	mov edx, esp     ; zerosLen is a constant, so we don't need to say
	                     ;  mov edx,[helloLen] to get it's actual value
	int 80h              ; Call the kernel

    jmp LP1

	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;

; Print::::

;Kill signal (SIGKILL)

;HeHelHellHelloHello Hello wHello woHello worHello worlHello worldHello world!Hello world!
;Hello world!
; .....