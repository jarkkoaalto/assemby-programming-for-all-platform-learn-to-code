;nasm 20.05.19
BITS 32

section .data
    hello: db 'Hello world!',10;
    helloLen: equ $-hello ;

section .bss
    var1: resb 4
  
section .text
	global _start

_start:
    mov esi, 9
    mov[var1], esi
    
    call PrintNumber
    
	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;
 
; Function
PrintNumber:
    mov esi, [var1]
    add esi, 48
    mov [var1], esi
    
    mov eax, 4
    mov ebx, 1
    mov ecx, var1
    mov edx, helloLen

	int 80h              ; Call the kernel
    
 ret
 
