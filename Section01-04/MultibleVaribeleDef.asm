;nasm 2.11.08
; resb - reserve byte - 1 byte
; resw - reserve word - 2 bytes
; resd - reserve fubleword - 4 bytes
; resq - reserve quardword - 8 bytes
; rest - reserve ten bytes - 10 bytes



section .data
    hello:     db 'Hello world!',10    ; 'Hello world!' plus a linefeed character
    helloLen:  equ $-hello             ; Length of the 'Hello world!' string
    var1: db "123"
    var2: db "456"

section .text
	global _start

_start:
	mov eax,4            ; The system call for write (sys_write)
	mov ebx,1            ; File descriptor 1 - standard output
	mov ecx,var1        ; Put the offset of hello in ecx
	mov edx,3     ; helloLen is a constant, so we don't need to say
	                     ;  mov edx,[helloLen] to get it's actual value
	int 80h              ; Call the kernel
 
	mov eax,4            ; The system call for write (sys_write)
	mov ebx,1            ; File descriptor 1 - standard output
	mov ecx,var2        ; Put the offset of hello in ecx
	mov edx,3    ; helloLen is a constant, so we don't need to say
	                     ;  mov edx,[helloLen] to get it's actual value
	int 80h              ; Call the kernel

	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;