; nasm

section .data
    hello: db   'Hello world!',10   ;
    helloLen:   equ $-hello         ;

    section .text
        global _start

    _start: ;       8421
        mov eax, 0b0100      ; immediate address mode
        mov ebx, 1          ; immediate address
        mov ecx, hello      ;
        mov edx, helloLen   ;

        int 80h             ;

        mov eax, 1          ; immediate address
        mov ebx, 0          ; immediate address
        int 80h;       