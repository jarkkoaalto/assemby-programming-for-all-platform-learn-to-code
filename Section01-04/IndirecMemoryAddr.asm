section .data
    hello:      db 'Hello world', 10    ;
    helloLen:   equ $-hello             ;

section .text
    qlobal _start

_start:
    mov abp, hello  ;
    mov eax 4       ;
    mov ebx,1       ;
    mov ecx, ebp    ;
    mov edx, helloLen;

    int 80h         ;

    mov eax,1       ;
    mov ebx,0       ;
    int 80h         ;