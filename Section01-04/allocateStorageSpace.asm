;nasm 20.05.19
;
; db = define byte - 1 byte
; dw = define word - 2 bytes
; dd = define doubleword - 4 bytes
; dq == define quadword - 8 bytes
; dt = define ten bytes - 10 bytes
;

section .data
   ; var1: db "Hi", 
    var1: dw "Hi";
    
section .text
    global _start
  
_start:
    mov eax,4;
    mov ebx,1;
    mov ecx,var1
    mov edx, 2
    
    int 80h;
    
    mov eax,1;
    mov ebx,0;
    int 80h;