;nasm 20.05.19

section .data
    zeros: TIMES 30 db "0"
    zerosLen: equ $-zeros
    
section .text
	global _start

_start:
	mov eax,4            ; The system call for write (sys_write)
	mov ebx,1            ; File descriptor 1 - standard output
	mov ecx,zeros        ; Put the offset of hello in ecx
	mov edx,zerosLen     ; zerosLen is a constant, so we don't need to say
	                     ;  mov edx,[helloLen] to get it's actual value
	int 80h              ; Call the kernel

	mov eax,1            ; The system call for exit (sys_exit)
	mov ebx,0            ; Exit with return code of 0 (no error)
	int 80h;